import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import 'package:zipdev/src/data/interfaces/iauth_repository.dart';
import 'package:zipdev/src/data/services/auth_service.dart';
import 'package:zipdev/src/models/user_model.dart';

part 'auth_event.dart';

part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthService _authService;

  AuthBloc(IAuthRepository repository) : super(AuthVerifyUserSessionState()) {
    _authService = AuthService(repository);
  }

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    yield AuthLoadingState();
    switch (event.runtimeType) {
      case AuthInitialStateEvent:
        yield AuthInitialState();
        break;
      case AuthLoginEvent:
        yield* _mapLoginToState(event);
        break;
      case AuthVerifyUserSessionEvent:
        yield* _mapVerifyUserSessionToState(event);
        break;
    }
  }

  Stream<AuthState> _mapLoginToState(AuthLoginEvent event) async* {
    final user = await _authService.login(event.email, event.password);
    yield AuthSuccessState(user);
  }

  Stream<AuthState> _mapVerifyUserSessionToState(
      AuthVerifyUserSessionEvent event) async* {
    final userLoggedIn = await _authService.userLoggedIn();
    final showWelcome = await _authService.showWelcomeScreen();
    yield userLoggedIn
        ? AuthSuccessState(UserModel())
        : AuthInitialState(showWelcome: showWelcome);
  }
}
