part of 'pokemon_bloc.dart';

abstract class PokemonEvent {
  PokemonEvent();
}

class PokemonInitialStateEvent extends PokemonEvent {
  PokemonInitialStateEvent();

  List<Object> get props => [];
}

class RetrievePokemonEvent extends PokemonEvent {
  final int page;
  final int itemsPerPage;

  RetrievePokemonEvent({this.page = 0, this.itemsPerPage = 10});

  List<Object> get props => [page, itemsPerPage];
}
