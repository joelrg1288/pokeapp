import 'package:zipdev/src/models/user_model.dart';

abstract class IAuthRepository {
  Future<bool> userLoggedIn();

  Future<bool> showWelcomeScreen();

  Future<UserModel> login(String email, String password);
}
