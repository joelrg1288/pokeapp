import 'package:zipdev/src/models/pokemon_model.dart';

abstract class IPokemonRepository {
  Future<PokemonModel> retrieveOnePokemon(String path);

  Future<List<PokemonModel>> retrievePokemon(int page, int itemsPerPage);
}
