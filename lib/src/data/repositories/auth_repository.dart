import 'dart:convert';

import 'package:zipdev/src/models/user_model.dart';
import 'package:zipdev/src/data/interfaces/iauth_repository.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthRepository extends IAuthRepository {
  @override
  Future<bool> userLoggedIn() async {
    final FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();
    final user = await flutterSecureStorage.read(key: 'zipdevUser');
    return user != null;
  }

  @override
  Future<bool> showWelcomeScreen() async {
    final FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();
    final showWelcome =
        await flutterSecureStorage.read(key: 'zipdevShowWelcome');
    if (showWelcome == null) {
      await flutterSecureStorage.write(key: 'zipdevShowWelcome', value: 'true');
      return true;
    }
    return false;
  }

  @override
  Future<UserModel> login(String email, String password) async {
    final user = UserModel(
      email: email,
      id: utf8.encode('$email:$password').join(''),
    );
    final FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();
    await flutterSecureStorage.write(
        key: 'zipdevUser', value: json.encode(user.toMap()));
    return user;
  }
}
