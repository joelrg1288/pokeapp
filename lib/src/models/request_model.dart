import 'package:flutter/material.dart';

enum HTTPMethod { GET, POST, PATCH, DELETE, PUT }

class RequestModel {
  final String path;
  final dynamic body;
  final HTTPMethod method;
  final Map<String, String> headers;

  RequestModel({
    this.body,
    @required this.path,
    @required this.method,
    this.headers = const {},
  });
}
