import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:zipdev/src/blocs/auth/auth_bloc.dart';
import 'package:zipdev/src/pages/login/widgets/header.dart';
import 'package:zipdev/src/pages/login/widgets/zipdev_form_field.dart';
import 'package:zipdev/src/utils/validators.dart';
import 'package:zipdev/src/widgets/zipdev_button.dart';

class LoginPage extends StatelessWidget {
  final _form = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void onLogin(AuthBloc authBloc, String email, String password) {
    if (_form.currentState.validate()) {
      authBloc.add(AuthLoginEvent(
        email: _emailController.value.text,
        password: _passwordController.value.text,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (_, AuthState state) {
        if (state is AuthSuccessState) {
          Navigator.pushReplacementNamed(context, 'pokemon-list');
        }
      },
      builder: (context, state) => Scaffold(
        body: Stack(
          children: <Widget>[
            Header(),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.1),
                child: Form(
                  key: _form,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Flexible(
                          child: FractionallySizedBox(
                            widthFactor: 0.7,
                            child: Text(
                              'PokeApp',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 40,
                                color: Colors.blue,
                                fontFamily: 'Pokemon',
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 80),
                        Flexible(
                          child: FractionallySizedBox(
                            widthFactor: 0.7,
                            child: ZipdevFormField(
                              label: 'Email',
                              controller: _emailController,
                              validator: Validators.emailValidator,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Flexible(
                          child: FractionallySizedBox(
                            widthFactor: 0.7,
                            child: ZipdevFormField(
                              obscure: true,
                              label: 'Password',
                              controller: _passwordController,
                              validator: Validators.passwordValidator,
                              textInputType: TextInputType.visiblePassword,
                            ),
                          ),
                        ),
                        SizedBox(height: 100),
                        ZipdevButton(
                          text: 'Sign in',
                          callback: () => onLogin(
                            BlocProvider.of<AuthBloc>(context),
                            '',
                            '',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: FractionallySizedBox(
                widthFactor: 0.45,
                child: Padding(
                  padding: EdgeInsets.only(top: 80),
                  child: Image(image: AssetImage('assets/images/pikachu.png')),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
