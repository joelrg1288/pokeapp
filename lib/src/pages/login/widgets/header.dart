import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: CustomPaint(
        painter: _HeaderPainter(),
      ),
    );
  }
}

class _HeaderPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final pencil = new Paint();
    pencil.color = Color(0xFFFF632B);
    pencil.style = PaintingStyle.fill;
    final Path path = _wave(size);
    canvas.drawPath(path, pencil);
  }

  Path _wave(Size size) {
    final path = new Path();
    path.moveTo(0, size.height * 0.28);
    path.quadraticBezierTo(size.width * 0.15, size.height * 0.35,
        size.width * 0.55, size.height * 0.25);
    path.quadraticBezierTo(
        size.width * 0.75, size.height * 0.2, size.width, size.height * 0.25);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    return path;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
