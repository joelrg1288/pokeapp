import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:zipdev/src/blocs/pokemon/pokemon_bloc.dart';
import 'package:zipdev/src/pages/pokemon-list/widgets/pokemon_card.dart';

class PokemonListPage extends StatelessWidget {
  final List<Widget> pokemonList = [];
  final ScrollController _controller = ScrollController();

  void onRetrieveMorePokemon(PokemonBloc pokemonBloc, int page) =>
      pokemonBloc.add(RetrievePokemonEvent(page: page));

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PokemonBloc, PokemonState>(
      listener: (_, PokemonState state) {},
      builder: (context, state) {
        if (state is PokemonInitialState) {
          this.onRetrieveMorePokemon(BlocProvider.of<PokemonBloc>(context), 0);
        }
        state.pokemon.forEach((e) => pokemonList.add(PokemonCard(
              pokemonModel: e,
            )));
        return Scaffold(
          body: Container(
            width: double.infinity,
            child: NotificationListener(
              onNotification: (notification) {
                if (notification is ScrollNotification) {
                  if (_controller.offset >=
                      _controller.position.maxScrollExtent) {
                    if (state is RetrievePokemonState)
                      this.onRetrieveMorePokemon(
                          BlocProvider.of<PokemonBloc>(context),
                          state.page + 1);
                  }
                }
                return true;
              },
              child: SingleChildScrollView(
                controller: _controller,
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    SafeArea(
                      child: Container(
                        width: double.infinity,
                        child: Wrap(
                          runSpacing: 15,
                          alignment: WrapAlignment.spaceAround,
                          children: pokemonList,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
