import 'package:flutter/material.dart';

import 'package:zipdev/src/widgets/zipdev_button.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Flexible(
                child: FractionallySizedBox(
                  widthFactor: 0.7,
                  child: Image(
                    image: AssetImage('assets/images/logo.png'),
                  ),
                ),
              ),
              SizedBox(height: 50),
              Flexible(
                child: FractionallySizedBox(
                    widthFactor: 0.9,
                    child: Text(
                      'PokeApp allows you to see the information of every existing pokemon, are you ready?',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 25,
                        letterSpacing: 5,
                        color: Colors.black54,
                        fontFamily: 'Pokemon',
                      ),
                    )),
              ),
              SizedBox(height: 100),
              ZipdevButton(
                text: 'I want to start!',
                callback: () =>
                    Navigator.pushReplacementNamed(context, 'login'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
